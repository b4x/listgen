#!/usr/bin/python3

import sys

def letterNum(word):
    vowel_num = {"a":"4", "e":"3", "i":"1", "o":"0"}
    result_word = ""
    for let in word:
        if let in vowel_num.keys():
            let = vowel_num[let]
        result_word = result_word + let
    return result_word

def mixer(word):
    result = []
    letter_num = letterNum(word)
    if letter_num != word:
        result.append(word)
    result.append(letter_num)
    print("Mixer result: " + str(result))
    return result

def loadIn(filename):
    fl_in = open(filename, "r")
    words = {}
    for word in fl_in:
        word = word.strip()
        print("Reading word: " + word)
        words[word] = mixer(word)
    fl_in.close()
    return words

def loadOut(filename, words_dic):
    fl_out = open(filename, "w")
    for word in words_dic.keys():
        for w in words_dic[word]:
            fl_out.write(w + "\n")
    fl_out.close()
    return True

def help():
    print("python3 "+ sys.argv[0] + " [base-wordlist] [out-wordlist]")
    print("default out-wordlist filename --> out.txt")

def main(argv):
    word_in = "in.txt"
    if len(argv) > 1:
        word_in = argv[1]
    if len(argv) == 3:
        word_out = argv[2]
    else:
        word_out = "out.txt"

    print("Leyendo fichero de entrada: " + word_in)
    wds = loadIn(word_in)
    print("COMPLETADA LA LECTURA DEL FICHERO")
    print("Creando fichero de salida: " + word_out)
    loadOut(word_out, wds)
    print("COMPLETADA LA CREACION DEL FICHERO")
    print("Pr0c3s0 3x1t0s0!")

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        help()
    else:
        main(sys.argv)

